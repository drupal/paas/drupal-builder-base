#!/bin/sh
set -e

echo "---> Setting up Nginx configuration..."
if [ -n "$(ls -A /tmp/nginx-configmap)" ]
then
  cp /tmp/nginx-configmap/nginx.conf /etc/nginx/nginx.conf
  cp /tmp/nginx-configmap/default.conf /etc/nginx/conf.d/default.conf
fi

exec nginx