# Based on https://github.com/docker-library/drupal/blob/master/8.6/fpm-alpine/Dockerfile
FROM php:7.2.10-fpm-alpine3.8

LABEL io.k8s.description="Drupal site builder base image" \
      io.k8s.display-name="Drupal Site Builder Base" \
      io.openshift.tags="builder,drupal,php,nginx,base" \
      maintainer="Drupal Admins <drupal-admins@cern.ch>"

# Copy the scripts to /usr/libexec/s2i when making the builder image
# This will be used also together with s2i scripts on the child image
# Also add any overriden entrypoint
COPY ./configuration/ ./entrypoints/ /usr/libexec/s2i/
RUN chmod -R +x /usr/libexec/s2i/

# install some utils
RUN apk --update add \
    # Some composer packages need git    
    git \
    patch \
    curl \
    gettext \
    zip \
    unzip \
	mysql-client \
	jq \
	nginx=1.14.0-r1 \
	tzdata	

# Configured timezone.
ENV TZ=Europe/Zurich
RUN touch /usr/share/zoneinfo/$TZ \
	&& cp /usr/share/zoneinfo/$TZ /etc/localtime \
	&& echo $TZ > /etc/timezone && \
	apk del tzdata \
	&& rm -rf /var/cache/apk/*

# NGINX
# Path configuration
RUN mkdir -p /etc/nginx/conf.d \
	# Needed for nginx.pid
	/var/run/ \
    /run/nginx/ \
    /var/log/nginx/ \
    /var/cache/nginx \
    /var/lib/nginx/ \
    /var/tmp/nginx/

# PHP-FPM
# install the PHP extensions we need
RUN set -ex \	
	&& apk add --no-cache --virtual .build-deps autoconf g++ make \
		coreutils \
		freetype-dev \
		libjpeg-turbo-dev \
		libpng-dev \
	# install redis
	&& pecl install -o -f redis \
	&& rm -rf /tmp/pear \
	&& docker-php-ext-enable redis \
	&& docker-php-ext-configure gd \
		--with-freetype-dir=/usr/include/ \
		--with-jpeg-dir=/usr/include/ \
		--with-png-dir=/usr/include/ \
	&& docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		zip \
	&& runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)" \
	&& apk add --virtual .drupal-phpexts-rundeps $runDeps \
	&& apk del .build-deps

# NGINX
# Setting permissions
RUN /usr/libexec/s2i/nginx-conf.sh

# PHP-FPM
# Setting permissions
RUN /usr/libexec/s2i/php-fpm-conf.sh

# Install composer
ENV COMPOSER_VERSION 1.7.2
ENV COMPOSER_HOME /var/www/html
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION}