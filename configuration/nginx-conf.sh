#!/bin/sh

echo "---> Setting permissions for Nginx folders..."

# We need to configmap:
# /etc/nginx/nginx.conf
# /etc/nginx/conf.d/default.conf
chown -R 1001:0	/etc/nginx/ \
                    /var/run/ \
                    /run/nginx/ \
                    /var/log/nginx/ \
                    /var/cache/nginx/ \
                    /var/lib/nginx/ \
                    /var/tmp/nginx/ \
&& chmod -R ug+rwx 	/etc/nginx/ \
                    /var/run/ \
                    /run/nginx/ \
                    /var/log/nginx \
                    /var/cache/nginx/ \
                    /var/lib/nginx/ \
                    /var/tmp/nginx/

echo "---> Setting permissions for Nginx folders... [DONE]"