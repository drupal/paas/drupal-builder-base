#!/bin/sh

echo "---> Setting permissions for PHP-FPM folders..."

# PHP-FPM directories
chown -R 1001:0 /usr/local/etc/php-fpm.d/ \
					/usr/local/etc/php/conf.d/ \
&& chmod -R ug+rwx /usr/local/etc/php-fpm.d/ \
				   /usr/local/etc/php/conf.d/

echo "---> Setting permissions for PHP-FPM folders... [DONE]"